#pragma once

#include <string>
#include <vector>
#include <iterator>
#include <algorithm>

#include "types.cpp"
#include "config.cpp"

#include <glibmm/ustring.h>

//template<class chartype=Glib::gunichar>
class Layout {
	typedef Glib::ustring string;
	typedef std::vector<string::value_type> vector;
public:
	typedef vector::iterator iterator;
	typedef vector::reverse_iterator reverse_iterator;
	typedef vector::const_iterator const_iterator;
	typedef vector::const_reverse_iterator const_reverse_iterator;

	Layout(const std::string &s = "") {
		this->layout.reserve(12+11+10); // 33 keys

		this->layout.clear();
		this->add(s);
	}
	virtual ~Layout() {}
	//Layout(const Layout &l) {}
	//Layout &operator=(const Layout &l) {}

	void add(const string &s) {
		copy(s.begin(), s.end(), std::back_inserter(this->layout));
	}
	void add(const std::string &s) {
		this->add(string(s));
	}
	void addLine(const std::string &s) {
		this->add(s);
		this->layout.push_back('\n');
	}

	void add(const string::value_type c) {
		this->layout.push_back(c);
	}

	// TODO write a back_inserter
	const std::string toString() const {
		std::string s;

		vector::const_iterator it = this->layout.begin();
		vector::const_iterator end = this->layout.end();
		vector::const_iterator block_end;

		for(block_end = it+5; it != block_end && it != end; ++it) {
			s.append(string(1, *it)); }
		s.append(" ");

		for(block_end = it+7; it != block_end && it != end; ++it) {
			s.append(string(1, *it)); }
		s.append("\n");

		for(block_end = it+5; it != block_end && it != end; ++it) {
			s.append(string(1, *it)); }
		s.append(" ");

		for(block_end = it+6; it != block_end && it != end; ++it) {
			s.append(string(1, *it)); }
		s.append("\n");

		for(block_end = it+5; it != block_end && it != end; ++it) {
			s.append(string(1, *it)); }
		s.append(" ");

		for(block_end = it+5; it != block_end && it != end; ++it) {
			s.append(string(1, *it)); }

		return s;
	}
	const string toUString() const {
		string s;
		for(vector::const_iterator it = this->layout.begin();
		it != this->layout.end(); ++it) {
			s.append(string(1, *it));
		}
		return s;
	}


	// get position and cost for key
	pos_t pos_of_key(string::const_reference s) const {
		pos_t pos;
		pos.cost = 0;

		// TODO bad style
		/*vector::const_iterator it = find(this->layout.begin(), this->layout.end(), s);
		// not found
		if(it == this->layout.end()) return pos;*/

		unsigned int i = 0;
		for(vector::const_iterator it = this->layout.begin();
			it != this->layout.end(); ++it) {
			//std::cout << *it << " " << s << std::endl;
			if(*it == s) break;
			++i;
		}
		// not found
		if(i == this->layout.size()) {
			pos.row = -1;
			return pos;
		}
		// else
		pos.cost = COST_PER_KEY[i];
		pos.row = 0;

		return pos;
	}

	// random access
	string::reference operator[](const int i) {
		return this->layout[i];
	}
	string::const_reference operator[](const int i) const {
		return this->layout[i];
	}

	string::reference operator[](const pos_t pos) {
		// TODO
		return this->operator()(pos.row, pos.col);
	}
	// matrix access
	string::reference operator()(const int row, const int col) {
		switch(row) {
			case 2: return this->layout[12+11+col];
			case 1: return this->layout[12+col];
			case 0:
			default:
				return this->layout[col];
		}
	}
	string::const_reference operator()(const int row, const int col) const {
		switch(row) {
			case 2: return this->layout[12+11+col];
			case 1: return this->layout[12+col];
			case 0:
			default:
				return this->layout[col];
		}
	}

	// return an iterator on the vector
	// iterates gunichars, need to be converted before sending to stream
	iterator begin() { return this->layout.begin(); }
	iterator end() { return this->layout.end(); }
	reverse_iterator rbegin() { return this->layout.rbegin(); }
	reverse_iterator rend() { return this->layout.rend(); }

	const_iterator begin() const { return this->layout.begin(); }
	const_iterator end() const { return this->layout.end(); }
	const_reverse_iterator rbegin() const { return this->layout.rbegin(); }
	const_reverse_iterator rend() const { return this->layout.rend(); }
private:
	// a vector storing keys to allow random (write) access
	// only stores a single char per position
	vector layout;
};
