// TODO: CSV export

#include <string>
#include <list>
#include <iostream>

#include "Layout.cpp"
#include "File.cpp"
#include "ngrams.cpp"

#include "layout_cost.cpp"

using namespace std;

typedef list<Layout> layoutlist;

const string evolvedline("# Evolved Layout ");

// return a list of all layouts in the file
// TODO better memory management
layoutlist getAllLayouts(const string &file) {
	layoutlist l;
	File f(file);

	for(string line; getline(f.stream(), line);) {
		if(line == evolvedline) {
			Layout layout;
			// next 6 blocks are our layout
			for(int lines = 6;
			lines && f.stream() >> line; --lines) {
				layout.add(line);
			}
			l.push_back(layout);
		}
	}

	return l;
}


// TODO implement
layoutlist getAllLayoutsInTextFilesIn(const string &folder = "results") {
	throw new string("Not Implemented");
	layoutlist l;
	return l;
}

// TODO external configuration options
int main(int argc, char **argv) {
	// check options
	if(argc < 2) {
		cerr << "Usage: " << argv[0] << " <filename>" << endl;
		return 1;
	}

	string file(argv[1]);
	layoutlist l = getAllLayouts(file);

	NGram ngrams1("1gramme.txt");

	// debugging output
	for(layoutlist::const_iterator it = l.begin();
		it != l.end(); ++it) {
		cout << evolvedline << endl;
		cout << position_cost(*it, ngrams1) << endl;
		/*cout << Glib::ustring(1, it->operator()(0, 3)).raw();
		cout << Glib::ustring(1, it->operator()(1, 3)).raw();
		cout << Glib::ustring(1, it->operator()(2, 3)).raw() << endl;*/
		cout << it->toString() << endl;
	}

	return 0;
}
