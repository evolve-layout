#pragma once

#include <fstream>

class File {
public:
	File(const std::string &name)
	: _stream(name.c_str()) {
		if(!_stream.is_open()) {
			//throw new std::runtime_error("Error opening file");
		}
	}
	virtual ~File() {
		this->_stream.close();
	}

	std::fstream &stream() {
		return this->_stream;
	}
private:
	std::fstream _stream;
	// no copying
	File(const File &);
	File &operator=(const File &);
};
