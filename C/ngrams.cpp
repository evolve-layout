#pragma once

#include <iostream>
#include <fstream>
#include <sstream>

#include <map>
#include <string>

struct NGram: public std::map<std::string, long> {
	NGram(const std::string &file) {
		std::ifstream f(file.c_str());

		long frequency;
		for(std::string gram; f >> frequency;) {
			f.get(); // drop one space
			getline(f, gram); // get rest of line

			this->insert(make_pair(gram, frequency));
		}

		f.close();
	}
};


/*int main(int argc, char **argv) {
	// check options
	if(argc < 2) {
		cerr << "Usage: " << argv[0] << " <filename>" << endl;
		return 1;
	}

	NGram ngrams(argv[1]);

	for(NGram::const_iterator it = ngrams.begin();
	it != ngrams.end(); ++it) {
		cout << it->first << " " << it->second << endl;
	}

	return 0;
}*/
