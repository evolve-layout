#include <sstream>
#include <string>
#include <fstream>

#include <glibmm/ustring.h>

#include "Layout.cpp"

using namespace std;
using namespace Glib;

int main(int argc, char **argv) {
	// dummy neo string
	Layout neo("# Evolved Layout\nxvlcw khgfqß´\nuiaeo snrtdy\nüöäpz bm,.j\n");


	ofstream f("test.txt");

	// print each char on its own line
	for(Layout::iterator it = neo.begin();
	it != neo.end(); ++it) {
		f << '"' << ustring(1, *it).raw() << '"' << endl;
	}

	f.close();

	return 0;
}
